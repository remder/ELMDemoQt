#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class QAbstractButton;
class ClassifierController;
class QDoubleSpinBox;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    int getClassifierNum();
    QColor getClassifierColor(int type);
    uint getClassifierCnt();
    ClassifierController* getController();
    uint hiddenNodeCnt();

signals:
    void sigResetSurface();
    void sigClearSamples();
public slots:
    void onStart();
    void onClassCntChanged(int cnt);
    void onResetSurface();
    void afterStopTrain();
    void onClearSamples();
    void onActionSaveTrigged();
    void onActionOpenTrigged();

private:
    Ui::MainWindow *ui;
    void preStartTrain();

};

#endif // MAINWINDOW_H
