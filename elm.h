#ifndef ELM_H
#define ELM_H
#include <QObject>
#include <matrix.h>
using namespace std;
using namespace splab;
class ELM:public QObject
{
    Q_OBJECT
public:
    ELM(uint inputNodeCnt,uint outputNodeCnt,uint hiddenNodeCnt);
    ~ELM();
    uint sampleSize() const;
    void setSampleSize(const uint &sampleSize);
    void randomWeightAndBias();
    bool train();
    void setInput(uint num,double* data);
    void setOutput(uint num,double* data);
    void test(double* sample);

    double *testOutput() const;
    double testOutput(uint num);

signals:
    void sigErr();

private:
    uint mInputNodeCnt;
    uint mOutputNodeCnt;
    uint mHiddenNodeCnt;
    uint mSampleSize;
    double** mWeight;
    double* mBias;
    double** mInput;
    double** mOutput;
    double* mTestOutput;
    Matrix<double> mH;
    Matrix<double> mTestH;
    Matrix<double> mTestT;
    Matrix<double> mBeta;
    Matrix<double> mT;
    double sigmoid(double v);
};

#endif // ELM_H
