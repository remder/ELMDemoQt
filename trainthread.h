#ifndef TRAINTHREAD_H
#define TRAINTHREAD_H

#include <QThread>
#include <QPixmap>

class MainWindow;
class Surface;
class ELM;
class TrainThread : public QThread
{
    Q_OBJECT
public:
    TrainThread(MainWindow* window,Surface* surface,ELM* network);

signals:
    void sigTrainFinished();
    void sigMap(QPixmap map);
private:
    MainWindow* mWindow;
    Surface* mSurface;
    ELM* mNetwork;
    QRgb* mColors;
    uint mClassCnt;
    QRgb getColor(double *output);
    double findMax(double *d, int *i);
    void setData(uchar *data, int x, int y, int width, uchar r, uchar g, uchar b);
protected:
    void run();
};

#endif // TRAINTHREAD_H
