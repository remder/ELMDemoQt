# ELMDemoQt

#### 项目介绍

使用Qt编写的 __超限学习机__ __(ELM)__ 分类器演示程序

#### 演示效果

![训练前](https://images.gitee.com/uploads/images/2018/1203/221043_85f08c25_1729038.png "屏幕截图.png")

![训练后](https://images.gitee.com/uploads/images/2018/1203/221100_76c3331e_1729038.png "屏幕截图.png")


#### 特性

1.支持隐层节点数调节

2.最多支持6类样本的分类

#### ELM相关知识

请移步[ELM官方网站](http://www.ntu.edu.sg/home/egbhuang/elm_codes.html)查看

#### 鸣谢

张明老师提供的SP++3.0数值计算与信号处理库，本程序用其求矩阵广义逆函数来计算H矩阵的广义逆，谢谢。

#### 姊妹篇

[BP神经网络分类器演示程序](https://gitee.com/ZzqiZQute/BPNNDemoQt)