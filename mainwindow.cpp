#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "surfacenode.h"
#include "classifierselector.h"
#include "classifiercontroller.h"
#include <QDebug>
#include <QDoubleSpinBox>
#include <QButtonGroup>
#include <QScreen>
#include <QRegExpValidator>
#include <QFileDialog>
#include <QDataStream>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->btnTrain,SIGNAL(clicked()),this,SLOT(onStart()));
    connect(ui->classifierController,SIGNAL(classCntChanged(int)),this,SLOT(onClassCntChanged(int)));
    connect(ui->btnResetSurface,SIGNAL(clicked()),this,SLOT(onResetSurface()));
    connect(ui->btnClearSamples,SIGNAL(clicked()),this,SLOT(onClearSamples()));
    connect(ui->actionSave,SIGNAL(triggered()),this,SLOT(onActionSaveTrigged()));
    connect(ui->actionOpen,SIGNAL(triggered()),this,SLOT(onActionOpenTrigged()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

int MainWindow::getClassifierNum()
{
    return ui->classifierController->getClassifierNum();
}


QColor MainWindow::getClassifierColor(int type)
{
    return ui->classifierController->getClassifierColor(type);
}

uint MainWindow::getClassifierCnt()
{
    return static_cast<uint>(ui->classifierController->getClassifierCnt());
}

ClassifierController *MainWindow::getController()
{
    return ui->classifierController;
}

uint MainWindow::hiddenNodeCnt()
{
    return static_cast<uint>(ui->sbHiddenNodeCnt->value());
}


void MainWindow::afterStopTrain()
{


    ui->btnTrain->setEnabled(true);
    ui->btnResetSurface->setEnabled(true);
    ui->classifierController->setAddDelButtonEnabled(true);
}

void MainWindow::onClearSamples()
{
    emit sigClearSamples();
}

void MainWindow::onStart()
{
    preStartTrain();
    ui->surface->startTrain();
}

void MainWindow::onClassCntChanged(int cnt)
{
    ui->surface->deleteNodeByClassCnt(cnt);
}


void MainWindow::onResetSurface()
{
    emit sigResetSurface();
}

void MainWindow::preStartTrain()
{
    ui->btnTrain->setEnabled(false);
    ui->classifierController->setAddDelButtonEnabled(false);
    ui->btnResetSurface->setEnabled(false);
    ui->classifierController->setCurrentClassNum(-1);
}


void MainWindow::onActionSaveTrigged()
{

    QString filename=QFileDialog::getSaveFileName(this,"保存文件",QString(),"Data (*.dat)");
    if(!filename.isEmpty()){
        if(!filename.endsWith(".dat")){
            filename+=".dat";
        }
        QFile file(filename);
        if (file.open(QIODevice::WriteOnly))
        {
            QDataStream in(&file);
            int cnt = ui->classifierController->getClassifierCnt();
            in<<cnt;
            cnt=ui->surface->nodeList().count();
            in<<cnt;
            for(int i=0;i<cnt;i++){
                SurfaceNode node=ui->surface->nodeList()[i];
                in<<node.type();
                in<<node.pos().x();
                in<<node.pos().y();
            }

        }

    }

}

void MainWindow::onActionOpenTrigged()
{
    QString filename=QFileDialog::getOpenFileName(this,"打开文件",QString(),"Data (*.dat)");
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly))
    {
        QDataStream out(&file);
        int cnt;
        out>>cnt;
        ui->classifierController->setClassifierNum(cnt);
        out>>cnt;
        QList<SurfaceNode> li;
        for (int i=0;i<cnt;i++)
        {
            int type;
            qreal x,y;
            out>>type>>x>>y;
            SurfaceNode node(type,QPointF(x,y));
            li<<node;
        }
        ui->surface->setNodeList(li);
    }


    ui->surface->update();

}

